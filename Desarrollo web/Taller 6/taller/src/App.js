/* import React, { useEffect, Component } from 'react';
import { useForm } from 'react-hook-form';
import axios from 'axios'

var libros = [];
var tabla = [];
export default function App() {
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = data =>{
    console.log(data);
    axios.post('http://localhost:8000/api/libros',data)
    .then(res=>{
      console.log(res)
      cargar();
    })

  }
  const cargar = ()  =>{
    axios.get('http://localhost:8000/api/libro')
    .then(res=>{
      tabla = [];
      libros = res.data.libro
      console.log("cantidad: ",libros.length)
      for(var i=0;i<libros.length;i++){
        let t =(
        <tr>
          <td>{libros[i].nombre}</td>
          <td>{libros[i].autor}</td>
          <td>{libros[i].cantidad}</td>
        </tr>)
        tabla.push(t)
      }
      console.log("Tabla: ",tabla)
    })
  }

  

  useEffect(()=>{
    cargar();
    
  })


 

  console.log(errors);
  
  return (
    <div>
    <form onSubmit={handleSubmit(onSubmit)}>
      <input type="text" placeholder="nombre" name="nombre" ref={register({required: true})} />
      <input type="text" placeholder="autor" name="autor" ref={register({required: true})} />
      <input type="number" placeholder="cantidad" name="cantidad" ref={register({required: true})} />

      <input type="submit" />
    </form>
  
    <table>
      <thead>
      <tr>
        <th>nombre</th>
        <th>Autor</th>
        <th>Cantidad</th>
        <th>Actualizar</th>
        <th>Eliminar</th>
      </tr>
      </thead>
      <tbody>
        {tabla}
      </tbody>

    </table>
    </div>
  );
} */

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Bienvenido a React</h2>
        </div>
          <p className="App-intro">
            Lista de usuarios
          </p>
      </div>
    );
  }
}

export default App;
