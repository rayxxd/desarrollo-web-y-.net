import React, { useEffect, Component } from 'react';
import TodoPage from '../pages/todo'
import axios from 'axios'




/* var aux = false  */ 



class Todo extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            items: [],
            aux:false,
            newItemText:'',
            newItemAutor:'',
            newItemCantidad:0,
     
        }

        this.onNewItem = this.onNewItem.bind(this)
        this.onRemoItem = this.onRemoItem.bind(this)
        this.onChangeNewItemText = this.onChangeNewItemText.bind(this)
        this.onChangeNewItemAutor = this.onChangeNewItemAutor.bind(this)
        this.onChangeNewItemCantidad = this.onChangeNewItemCantidad.bind(this)
        this.guardar = this.guardar.bind(this)
       
      
    }

    onNewItem(){
        this.setState({
            items:[
                ...this.state.items,
                {
                    _id: Date.now(),
                    nombre:'',
                    autor:'',
                    cantidad:0,
                    isEditting:true,
                }
            ]
        })
    }
    
    componentDidMount(){
        axios.get('http://localhost:8000/api/libro').then(res=>{
            this.setState({items: res.data.libro})
            this.setState({aux: true})
            console.log("LIBROS: ",this.state.items)      
        })
    }
    onRemoItem(items,item){
        axios.delete(`http://localhost:8000/api/libro/${item}`).then(result=>{
            alert("Libro eliminado");
            this.setState({
              response:result,
              items:items.filter(item2=>item2._id !== item)
            });
        });
        console.log("LIBROS Despues de Eliminar: ",this.state.items)
    }

    onChangeNewItemText(event){

        this.setState({newItemText: event.target.value});
    }

    onChangeNewItemAutor(event){
        this.setState({newItemAutor: event.target.value});

    }
    onChangeNewItemCantidad(event){
        this.setState({newItemCantidad: event.target.value});
    }

    guardar(newItemText,newItemAutor,newItemCantidad){
        let data = {"nombre":newItemText,"autor":newItemAutor,"cantidad":newItemCantidad  }
        console.log(data);
        
        axios.post('http://localhost:8000/api/libros',data)
        alert("Libro agregado");
        window.location.reload(true);
    }

    render(){
        const{
            newItemText,
            newItemCantidad,
            newItemAutor,
        } = this.state;
       
        return (
            <TodoPage
                items = {this.state.items}
                
                onNewItem ={this.onNewItem}
                onRemoItem={this.onRemoItem}
                newItemText={newItemText}
                newItemAutor={newItemAutor}
                newItemCantidad={newItemCantidad}
                guardar={this.guardar}
                onChangeNewItemAutor={this.onChangeNewItemAutor}
                onChangeNewItemCantidad={this.onChangeNewItemCantidad}
                onChangeNewItemText={this.onChangeNewItemText}
          
            />

          
        );
        
    } 

}


export default Todo;