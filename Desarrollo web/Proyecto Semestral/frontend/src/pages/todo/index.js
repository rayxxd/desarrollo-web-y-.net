import React from 'react'

import axios from 'axios'
import Layout from '../../componente/layout';


import{
    Card,
    H2,
    Divider,
    UL,
    Button,
    Checkbox,
    Label,
    InputGroup,
    Table,
    Intent,
    Callout,
   
} from "@blueprintjs/core"
import './style.css'







function TodoPage(props){
   
    
   
    const {
        
        items,
        onNewItem,
        onRemoItem,
        newItemAutor,
        newItemCantidad,
        newItemText,
        onChangeNewItemText,
        onChangeNewItemAutor,
        onChangeNewItemCantidad,
        guardar,
        
        

    } = props;

   

    
  
    console.log("TODO: ", items);
    return(
    <div>
       <Layout>
           <Card>
                <H2>Proyecto Desarrollo web</H2>
                <Button
                    text="Nuevo Libro"
                    intent={Intent.PRIMARY}
                    icon="new-object"
                    className="new-todo-item"
                    onClick={onNewItem}
                />
               {/*  <Callout intent={Intent.DANGER}>
                    Error
                </Callout> */}
                <Divider/>   

                <UL className="todo-list">
                    {items.map(item =>
                        <li className="todo-item" key={item._id}>
                            <Button 
                                small
                                icon="remove"
                                intent="danger"
                                className="todo-item-action"
                                onClick= {() => onRemoItem(items,item._id)}
                            />

                            {item.isEditting ?
                                <React.Fragment>
                                <InputGroup
                                    small
                                    className="todo-item-text-edit"
                                    value={newItemText}
                                    placeholder="Libro"
                                    onChange={onChangeNewItemText}
                                />
                                <InputGroup
                                    small
                                    className="todo-item-text-edit"
                                    value={newItemAutor}
                                    placeholder="Autor"
                                    onChange={onChangeNewItemAutor} 
                                  
                                />
                                <InputGroup
                                    small
                                    type="number"
                                    className="todo-item-text-edit"
                                    value={newItemCantidad}
                                    placeholder="Cantidad"
                                    onChange={onChangeNewItemCantidad}
                                />
                                <Button
                                    text="Guardar"
                                    small
                                    intent={Intent.PRIMARY}
                                    className="new-todo-item"
                                    onClick= {() => guardar(newItemText,newItemAutor,newItemCantidad)}
                                
                                
                                />

                                

                               </React.Fragment>
                                :
                                <React.Fragment>
                                    

                                    <Label
                                        className={`todo-item-label ${item.isChecked ? 'done':''}`}
                                    >
                                        Libro: {item.nombre}/
                                    </Label>
                                    <Label
                                        className="todo-item-label label-autor"
                                        
                                    >
                                        Autor: {item.autor}/
                                    </Label>
                                    <Label
                                        className="todo-item-label label-autor"
                                    >
                                        Cantidad: {item.cantidad}

                                    </Label>
                                </React.Fragment>    
                            }
                        </li>
                    )}
                </UL>
           </Card>
       </Layout>
    </div>);
}

export default TodoPage;