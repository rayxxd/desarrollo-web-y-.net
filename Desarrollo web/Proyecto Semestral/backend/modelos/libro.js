'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema


const LibroSchema = Schema({

    nombre:String,
    autor:String,
    cantidad:{type:Number}

})

module.exports = mongoose.model('Libro',LibroSchema)