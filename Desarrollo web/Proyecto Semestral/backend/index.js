'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Libro = require('./modelos/libro')

const app = express()
app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Accept');
    next();
});


app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())



//Lista de Libros
app.get('/api/libro',(req,res)=>{

    Libro.find({}).then(function(libro){
        res.send({libro});
    });

})

//Un libro especifico
app.get('/api/libro/:id',(req,res)=>{

    let libroID = req.params.id
    Libro.findById(libroID,(err,libro)=>{
        if(err) return res.status(500).send({message:'Error al realizar la peticion'})
        if(!libro) return res.status(404).send({message:'Error el libro no existe'})
        
        res.status(200).send({libro})
    })

    

})

//Ingresamos un libro 
app.post('/api/libros',(req,res)=>{

    let libro = new Libro()
    libro.nombre = req.body.nombre
    libro.autor = req.body.autor
    libro.cantidad = req.body.cantidad

    libro.save((err,libroStore)=>{

        if(err) res.status(500).send(`Error al ingresar libro (DB): ${err}`)

        res.status(200).send({libro:libroStore})

    })
    
})

//Eliminar libro

app.delete('/api/libro/:id',(req,res)=>{
    let libroId = req.params.id
    Libro.findByIdAndDelete(libroId,(err,libro)=>{
        if(err) return res.status(500).send({message:'error al realizar peticion'})
        if(!libro) return res.status(404).send({message: 'Libro no existe'})
        res.status(200).send({libro})
    })
})

//Actualizar libro
app.put('/api/libro/:id', (req, res) => {
    let id = req.params.id;
  
    let body = req.body;
  
    Libro.findByIdAndUpdate(id, body, { new: true }, (err, libro) => {
      if (err) {
        return res.status(400).json({
          ok: false,
          err
        });
      }
  
      res.json({
        ok: true,
        libro
      });
    });
});



mongoose.connect(`mongodb+srv://rayx:mamertox@cluster0-y7t58.mongodb.net/test?retryWrites=true&w=majority`,(err,res)=>{

    if(err) throw (err)
    
    console.log("Conexion establecida")
    
    app.listen(8000,()=>{

        console.log("Esta corriendo en el puerto 8000")
    })


})


